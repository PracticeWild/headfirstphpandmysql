<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Guitar Wars - Add Your High Score</title>
    <link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<h2>Guitar Wars - Add Your High Score</h2>

<?php
require_once('appvars.php');
require_once('connectvars.php');
require_once('utils.php');

if (isset($_POST['submit'])) {
    // Grab the score data from the POST
    $name = $_POST['name'];
    $score = $_POST['score'];
    $screenshot = $_FILES['screenshot'];

    if (!empty($name) && !empty($score) && !empty($screenshot)) {
        if (is_valid_screenshot($screenshot)) {
            if (move_uploaded_file($screenshot['tmp_name'], GW_UPLOADPATH . $screenshot['name'])) {
                // Connect to the database
                $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

                // Write the data to the database
                $query = "INSERT INTO guitarwars VALUES (0, NOW(), '$name', '$score', '$screenshot[name]')";
                mysqli_query($dbc, $query);

                // Confirm success with the user
                echo '<p>Thanks for adding your new high score!</p>';
                echo '<p><strong>Name:</strong> ' . $name . '<br />';
                echo '<strong>Score:</strong> ' . $score . '</p>';
                echo '<strong>Screenshot:</strong><image src="' . GW_UPLOADPATH . $screenshot['name'] . '" alt="uploaded image"/><br/>';
                echo '<p><a href="index.php">&lt;&lt; Back to high scores</a></p>';

                // Clear the score data to clear the form
                $name = "";
                $score = "";
                $screenshot = null;

                mysqli_close($dbc);
            } else {
                echo '<p style="color:red">ERROR Saving file</p>';
            }
        } else {
            echo '<p style="color:red">Screenshot have to be gif, jpeg or png format and smaller than ' . GW_MAX_FILE_SIZE / 1024 . ' MB</p>';
        }
        @unlink($screenshot['tmp_name']);
    } else {
        echo '<p style="color:red">Please set a name, score and a screenshot</p>';
    }
}
?>

<hr/>
<form method="post" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo GW_MAX_FILE_SIZE; ?>"/>
    <label for="name">Name:</label>
    <input type="text" id="name" name="name" value="<?php if (!empty($name)) echo $name; ?>"/><br/>
    <label for="score">Score:</label>
    <input type="text" id="score" name="score" value="<?php if (!empty($score)) echo $score; ?>"/><br/>
    <label for="screenshot">Screenshot:</label>
    <input type="file" id="screenshot" name="screenshot"/><br/>
    <hr/>
    <input type="submit" value="Add" name="submit"/>
</form>
</body>
</html>
